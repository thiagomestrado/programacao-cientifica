#ifndef NO_H
#define NO_H
#include <string>
using namespace std;

class No
{
    public:
        No();
        No(string _estadoAtual);
        No(string _estadoAtual, No * _pai);
        virtual ~No();
        string estadoAtual;
        void determinarPai(No * pai);
        No * obterPai();
        void determinarNivel();
        int obterNivel();
        int valorDeH;
        int obterValorDeF();

    private:
        No * pai;
        int nivel; //Quando utilizando o algoritmo A* esse valor pode ser usado como o Valor de G.
};

#endif // NO_H
