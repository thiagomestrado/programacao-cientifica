#ifndef DFS_H
#define DFS_H
#include "JogoBase.h"

class DFS : JogoBase
{
    public:
        DFS(string _estadoInicial);
        vector<string> encontrarSolucao();

    private:
        static const int profundidadeMaxima;
        vector<string> jaVisitados;
        No* visitarFilhosDiretos(No * pai); //Método responsável por tratar a recursividade do DFS
};

#endif // DFS_H
