#include "Funcao.h"

Funcao::Funcao(double inicioIntegral, double fimIntegral, int funcao, int metodoEscolhido, int numeroDivisoes)
{
    this->inicioIntegral = inicioIntegral;
    this->fimIntegral = fimIntegral;
    this->funcaoEscolhida = funcao;
    this->metodoEscolhido = metodoEscolhido;
    this->numeroDivisoes = numeroDivisoes;
}

double Funcao::derivadaF1(double valorX)
{
    //Tanto Primeira quanto segunda derivada.
    return exp(valorX);
}

double Funcao::derivadaF2(double valorX)
{
    //Derivada Primeira
    /*double primeiraParte = -valorX;
    double segundaParte = sqrt(1 - pow(valorX, 2));
    return primeiraParte / segundaParte;*/

    //Derivada Segunda
    double primeiraParte = 1 - pow(valorX, 2);
    double segundaParte = pow(primeiraParte, (3 / 2));
    return -1 / segundaParte;
}

double Funcao::derivadaF3(double valorX)
{
    //Derivada Primeira
    /*double primeiraParte = -2 * valorX;
    double segundaParte = exp(pow(-valorX, 2));
    return primeiraParte * segundaParte;*/

    //Derivada Quarta
    double primeiraParte = (16 * pow(valorX, 4)) - (48 * pow(valorX, 2)) + 12;
    double segundaParte = exp(pow(-valorX, 2));
    return primeiraParte * segundaParte;
}

double Funcao::funcaoAvaliar1(double valorX)
{
    return exp(valorX);
}

double Funcao::funcaoAvaliar2(double valorX)
{
    double aux = pow(valorX, 2); //variavel para facilitar a representacao no proximo passo.
    return sqrt(1 - aux);
}

double Funcao::funcaoAvaliar3(double valorX)
{
    double aux = pow(-valorX, 2); //variavel para facilitar a representacao no proximo passo.
    return exp(aux);
}

double Funcao::calcularIntegral(metodosIntegral metodo, int numeroDivisoes, double *erro)
{
    double valorDoIntervaloParaIntegral = ((fimIntegral - inicioIntegral) / (double)numeroDivisoes);
    vector<double> valoresCalculados;

    for(int i = 0; i < numeroDivisoes; i++)
    {
        double aux = definirFuncaoParaResolver(inicioIntegral + (i * valorDoIntervaloParaIntegral));
        valoresCalculados.push_back(aux);
    }

    return (this->*metodo)(valoresCalculados, erro);
}

double Funcao::integracaoRetangulo(vector<double> valoresX, double *erro)
{
    cout<<endl<<"METODO RETANGULO"<<endl;
    double diferencaIntervalo = fimIntegral - inicioIntegral;

    double valorDaIntegral = (fimIntegral - inicioIntegral) * valoresX[1];

    double primeiraParteErro = - ((pow(diferencaIntervalo, 3)) / 24);
    *erro = primeiraParteErro * definirFuncaoDerivadaErro(valorDaIntegral);

    return valorDaIntegral;
}

double Funcao::integracaoTrapezio(vector<double> valoresX, double *erro)
{
    cout<<endl<<"METODO TRAPEZIO"<<endl;
    double diferencaIntervalo = fimIntegral - inicioIntegral;

    double valorDaIntegral = (diferencaIntervalo / 2) * (valoresX[0] + valoresX[1]);

    double primeiraParteErro =  - ((pow(diferencaIntervalo, 3)) / 12);
    *erro = primeiraParteErro * definirFuncaoDerivadaErro(valorDaIntegral);

    return valorDaIntegral;
}

double Funcao::integracaoSimpson(vector<double> valoresX, double *erro)
{
    cout<<endl<<"METODO SIMPSON"<<endl;
    double diferencaIntervalo = fimIntegral - inicioIntegral;

    double valorDaIntegral = ((fimIntegral - inicioIntegral) / 6) * (valoresX[0] + (4 * valoresX[1]) + valoresX[2]);

    double primeiraParteErro = -((pow(diferencaIntervalo, 5)) / 2880);
    *erro = primeiraParteErro * definirFuncaoDerivadaErro(valorDaIntegral);

    return valorDaIntegral;
}

double Funcao::definirFuncaoParaResolver(double valorX)
{
    if(funcaoEscolhida == 1)
    {
        return funcaoAvaliar1(valorX);
    }
    else if(funcaoEscolhida == 2)
    {
        return funcaoAvaliar2(valorX);
    }
    else
    {
        return funcaoAvaliar3(valorX);
    }
}

double Funcao::definirFuncaoDerivadaErro(double valorX)
{
    if(funcaoEscolhida == 1)
    {
        return derivadaF1(valorX);
    }
    else if(funcaoEscolhida == 2)
    {
        return derivadaF2(valorX);
    }
    else
    {
        return derivadaF3(valorX);
    }
}



