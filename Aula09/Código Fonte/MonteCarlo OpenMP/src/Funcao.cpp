#include "../include/Funcao.h"

Funcao::Funcao(double inicioIntegral, double fimIntegral, int funcaoEscolhida, int numeroDePontos)
{
    this->inicioIntegral = inicioIntegral;
    this->fimIntegral = fimIntegral;
    this->funcaoEscolhida = funcaoEscolhida;
    this->numeroDePontos = numeroDePontos;

    distribuicao = new uniform_int_distribution<int> (1, 100);
    random_device randomDevice;
    aleatorio = new mt19937(randomDevice());

    distribuicaoY = new uniform_int_distribution<int> (-3, 100);
    distribuicaoZ = new uniform_int_distribution<int> (1, 100);
    aleatorioY = new mt19937(2659);
    aleatorioZ = new mt19937(1698);
}

double Funcao::funcaoAvaliar1(double valorDeX)
{
    double divisor = 1 + pow(valorDeX, 2);
    return 4 / divisor;
}

double Funcao::funcaoAvaliar2(double valorDeX)
{
    if(valorDeX < 0)
        throw "Nao pode tirar raiz de numero negativo!";

    double raizDeDentro = sqrt(valorDeX);
    return sqrt(valorDeX + raizDeDentro);
}

double Funcao::funcaoAvaliar3(double valorDeX)
{
    valorDeX = valorDeX / 100;
    double valorDeY = (*distribuicaoY)(*aleatorioY) / 100;
    double valorDeZ = (*distribuicaoZ)(*aleatorioZ) / 100;

    double segundaParteFuncao = pow((sqrt(pow(valorDeX, 2) + pow(valorDeY, 2)) - 3), 2);
    return pow(valorDeZ, 2) + segundaParteFuncao;
}

double Funcao::calcularIntegral(EquacaoEscolhida funcaoParaResolver, double *erro)
{
    double somaDosValoresRandomicos = 0;
    double somaParaCalcularErro = 0;

    for(int i = 0; i < numeroDePontos; i++)
    {
        double valorDeXAleatorio = (*distribuicao)((*aleatorio)); //Obtem o numero aleatorio para calculo da funcao
        double valorResolvidoDaFuncao = (this->*funcaoParaResolver)(valorDeXAleatorio); //OBtem valor calculado da funcao

        somaDosValoresRandomicos += valorResolvidoDaFuncao;
        somaParaCalcularErro += pow(valorResolvidoDaFuncao, 2);
    }

    double resultadoAux = (somaDosValoresRandomicos / numeroDePontos); //Calcula o valor das funcoes dos numericos aleatorios dividido pelo numero de pontos.
    double resultado = (fimIntegral - inicioIntegral) * resultadoAux; //Resultado da integral

    double erroAux = ((somaParaCalcularErro / numeroDePontos) - pow(resultadoAux, 2)) / numeroDePontos;
    *erro = sqrt(erroAux);

    return resultado;
}
