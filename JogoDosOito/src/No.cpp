#include "No.h"

No::No(string _estadoAtual)
{
    estadoAtual = _estadoAtual;
    pai = 0;
    determinarNivel();
}

No::No(string _estadoAtual, No * _pai)
{
    estadoAtual = _estadoAtual;
    pai = _pai;
}

No::~No()
{
    estadoAtual = "";
    pai = 0;
}

void No::determinarPai(No * pai)
{
    this->pai = pai;
}

No * No::obterPai()
{
    return this->pai;
}

void No::determinarNivel()
{
    if(this->pai != 0)
    {
        int nivelPai = this->pai->nivel;
        this->nivel = nivelPai + 1;
    }
    else
    {
        this->nivel = 0;
    }
}

int No::obterNivel()
{
    return this->nivel;
}

int No::obterValorDeF()
{
    return this->nivel + this->valorDeH;
}
