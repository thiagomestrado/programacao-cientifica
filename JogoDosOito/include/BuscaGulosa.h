#ifndef BUSCAGULOSA_H
#define BUSCAGULOSA_H
#include "JogoBase.h"
#include "MelhorPrimeiro.h"

class BuscaGulosa : JogoBase
{
    public:
        BuscaGulosa(string _estadoInicial);
        vector<string> encontrarSolucao();

    private:
        vector<string> jaVisitados;
        vector<No*> paraVisitar;
};

#endif // BUSCAGULOSA_H
