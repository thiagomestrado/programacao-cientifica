#include "DescidaGradiente.h"

double DescidaGradiente::obterResultado(double valorDeX)
{
    double resultado, f0, si;
    int iteracao = 1;

    f0 = funcaoInicial(valorDeX);
    si = derivadaDaFuncaoInicial(valorDeX);

    do
    {
        valorDeX = valorDeX - (taxaDeAprendizado * si);
        f0 = funcaoInicial(valorDeX);
        si = derivadaDaFuncaoInicial(valorDeX);

        resultado = valorDeX;

        iteracao ++;
    }
    while(iteracao <= numeroMaximoDePassos &&
          abs(si) > criterioDeParada);

    if(iteracao > numeroMaximoDePassos)
        throw "Solucao nao encontrada!";

    cout<<"Resultado encontrado com "<<iteracao - 1<<" iteracoes!"<<endl;

    return resultado;
}
