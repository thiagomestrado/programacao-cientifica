#ifndef EXERCICIOBDESCIDA_H
#define EXERCICIOBDESCIDA_H
#include "DescidaGradiente.h"

class ExercicioBDescida : public DescidaGradiente
{
    public:
        double funcaoInicial(double valorDeX);
        double derivadaDaFuncaoInicial(double valorDeX);
};

#endif // EXERCICIOBDESCIDA_H
