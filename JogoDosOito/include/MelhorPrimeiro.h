#ifndef MELHORPRIMEIRO_H
#define MELHORPRIMEIRO_H
#include <string>
#include <vector>
#include <cmath>
#include "No.h"
#include "Ponto.h"

class MelhorPrimeiro
{
    public:
        MelhorPrimeiro();
        ~MelhorPrimeiro();
        void determinarValorDeH(No * estadoParaDeterminarValorDeH); //M�todo respons�vel por encontrar o valor de H utilizando a dist�ncia de Manhattan.
        No * verificarEstadoNaListaParaVisitar(string estadoParaVerificar, vector<No*> * listaParaFazerVerificao);
        No* encontrarEstadoComMenorFuncao(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados, bool buscaGulosa); //M�todo respons�vel por encontrar o N� com o menor valor da fun��o heur�stica.

    private:
        vector<Ponto*> pontosDoEstadoSolucao;
        void determinarPosicoesDasPecasNaListaDeSolucao();
        No * encontrarEstadoComMenorFuncaoBuscaGulosa(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados); //M�todo auxiliar para busca quando � Busca Gulosa
        No * encontrarEstadoComMenorFuncaoAEstrela(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados); //M�todo auxiliar para busca quando � A*
};

#endif // MELHORPRIMEIRO_H
