#include "ExercicioDDescida.h"

double ExercicioDDescida::funcaoInicial(double valorDeX, double valorDeY)
{
    return pow((1 - valorDeY), 2) + (100 * pow((valorDeX - (pow(valorDeY, 2))), 2));
}

double ExercicioDDescida::derivadaX(double valorDeX, double valorDeY)
{
    valorAux =  std::numeric_limits<double>::epsilon();
    return ((funcaoInicial(valorDeX + valorAux, valorDeY) - funcaoInicial(valorDeX - valorAux, valorDeY)) / valorAux);
}

double ExercicioDDescida::derivadaY(double valorDeX, double valorDeY)
{
    valorAux =  std::numeric_limits<double>::epsilon();
    return ((funcaoInicial(valorDeX, valorDeY + valorAux) - funcaoInicial(valorDeX, valorDeY - valorAux)) / valorAux);
}

double ExercicioDDescida::obterResultado(double valorDeX, double valorDeY)
{
    double resultado, gradientX, gradientY;
    int iteracao = 1;

    do
    {
        gradientX = derivadaX(valorDeX, valorDeY);
        gradientY = derivadaY(valorDeX, valorDeY);

        valorDeX = valorDeX - (taxaDeAprendizado * gradientX);
        valorDeY = valorDeY - (taxaDeAprendizado * gradientY);

        resultado = funcaoInicial(valorDeX, valorDeY);

        iteracao ++;
    }
    while(iteracao <= numeroMaximoDePassos &&
          abs(resultado) > criterioDeParada);

    if(iteracao > numeroMaximoDePassos)
        throw "Solucao nao encontrada!";

    cout<<"Resultado encontrado com "<<iteracao - 1<<" iteracoes!"<<endl;

    return resultado;
}
