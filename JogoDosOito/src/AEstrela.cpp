#include "AEstrela.h"

AEstrela::AEstrela(string _estadoInicial) : JogoBase(_estadoInicial)
{
}

vector<string> AEstrela::encontrarSolucao()
{
    No * estadoSolucaoFinal = 0;
    No * noDoEstadoInicial = new No(estadoInicial);
    MelhorPrimeiro melhorPrimeiro; //Composição

    if(compararSolucao(estadoInicial))
    {
        vector<string> solucao;
        solucao.push_back(estadoInicial);

        return solucao;
    }

    paraVisitar.push_back(noDoEstadoInicial);

    while(paraVisitar.size() > 0)
    {
        int* indexParaDeletar;
        No * visitando = melhorPrimeiro.encontrarEstadoComMenorFuncao(indexParaDeletar, &paraVisitar, false);

        jaVisitados.push_back(visitando->estadoAtual);

        paraVisitar.erase(paraVisitar.begin() + (*indexParaDeletar));

        if(compararSolucao(visitando->estadoAtual))
        {
            estadoSolucaoFinal = visitando;
            break;
        }

        vector<No *> filhos = criarNosFilhos(visitando);

        for(int i = 0; i < filhos.size(); i++)
        {
            No * filhoVisitando = filhos[i];

            if(verificarEstadoJaVisitado(filhoVisitando->estadoAtual, &jaVisitados))
            {
                continue;
            }

            //O valor de G pode ser o valor do nivel.
            No * filhoJaNaListaParaVisitar = melhorPrimeiro.verificarEstadoNaListaParaVisitar(filhoVisitando->estadoAtual, &paraVisitar);

            if(filhoJaNaListaParaVisitar != 0 && filhoJaNaListaParaVisitar->obterNivel() > filhoVisitando->obterNivel())
            {
                filhoJaNaListaParaVisitar->determinarPai(visitando);
                filhoJaNaListaParaVisitar->determinarNivel();
            }
            else
            {
                if(filhoJaNaListaParaVisitar == 0)
                {
                    melhorPrimeiro.determinarValorDeH(filhoVisitando);
                    paraVisitar.push_back(filhoVisitando);
                }
            }
        }
    }

    return obterEstadosDaSolucao(estadoSolucaoFinal);
}
