#ifndef DESCIDAGRADIENTE_H
#define DESCIDAGRADIENTE_H
#include <iostream>
#include <cmath>
using namespace std;

class DescidaGradiente
{
    public:
        virtual double funcaoInicial(double valorDeX) = 0;
        virtual double derivadaDaFuncaoInicial(double valorDeX) = 0;
        double obterResultado(double valorDeX);

    private:
        double taxaDeAprendizado = 0.2;
        int numeroMaximoDePassos = 200; //Quantidade maxima de iteracoes.
        double criterioDeParada = 0.0001; //Quer dizer que esta proximo suficiente de zero para desconsiderar mais iteracoes.
};

#endif // DESCIDAGRADIENTE_H
