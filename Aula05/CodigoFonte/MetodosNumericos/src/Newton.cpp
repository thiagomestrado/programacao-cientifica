#include "Newton.h"

double Newton::obterResultado(double valorDeX)
{
    double resultado, f0, si;
    int iteracao = 1;
    f0 = funcaoInicial(valorDeX);

    do
    {
        si = derivadaDaFuncaoInicial(valorDeX);
        valorDeX = valorDeX - (taxaDeAprendizado * f0 * (1 / si));
        f0 = funcaoInicial(valorDeX);

        resultado = valorDeX;
        iteracao ++;
    }
    while(iteracao <= numeroMaximoDePassos &&
          abs(f0) > criterioDeParada);

    if(iteracao > numeroMaximoDePassos)
        throw "Solucao nao encontrada!";

    cout<<"Resultado encontrado com "<<iteracao - 1<<" iteracoes!"<<endl;

    return resultado;
}
