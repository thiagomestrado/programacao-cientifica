#include "ExercicioCDescida.h"

double ExercicioCDescida::funcaoInicial(double valorDeX, double valorDeY)
{
    return pow((1 - valorDeX), 2) + pow((1 - valorDeY), 2);
}

double ExercicioCDescida::derivadaDaFuncaoInicial(double valorDeX)
{
    return (2 * valorDeX);
}

double ExercicioCDescida::derivadaX(double valorDeX, double valorDeY)
{
    valorAux =  std::numeric_limits<double>::epsilon();
    return ((funcaoInicial(valorDeX + valorAux, valorDeY) - funcaoInicial(valorDeX - valorAux, valorDeY)) / valorAux);
}

double ExercicioCDescida::derivadaY(double valorDeX, double valorDeY)
{
    valorAux =  std::numeric_limits<double>::epsilon();
    return ((funcaoInicial(valorDeX, valorDeY + valorAux) - funcaoInicial(valorDeX, valorDeY - valorAux)) / valorAux);
}

double ExercicioCDescida::obterResultado(double valorDeX, double valorDeY)
{
    double resultado, gradientX, gradientY;
    int iteracao = 1;

    do
    {
        gradientX = derivadaX(valorDeX, valorDeY);
        gradientY = derivadaY(valorDeX, valorDeY);

        valorDeX = valorDeX - (taxaDeAprendizado * gradientX);
        valorDeY = valorDeY - (taxaDeAprendizado * gradientY);

        resultado = funcaoInicial(valorDeX, valorDeY);

        iteracao ++;
    }
    while(iteracao <= numeroMaximoDePassos &&
          abs(gradientX) > criterioDeParada);

    if(iteracao > numeroMaximoDePassos)
        throw "Solucao nao encontrada!";

    cout<<"Resultado encontrado com "<<iteracao - 1<<" iteracoes!"<<endl;

    return resultado;
}
