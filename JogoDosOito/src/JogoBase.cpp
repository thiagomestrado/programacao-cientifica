#include "JogoBase.h"

JogoBase::JogoBase(string _estadoInicial)
{
    this->estadoInicial = _estadoInicial;
    this->paiInicial = new No(_estadoInicial);
}

JogoBase::~JogoBase()
{
    this->estadoInicial ="";
    this->paiInicial = 0;
}

const string JogoBase::estadoSolucao = "123456780";

//Zero (0) � a posi��o vazia.
const string JogoBase::posicaoVazia = "0";

vector<string> JogoBase::encontrarProximosEstados(string estadoAtual)
{
    vector<string> proximosEstados;
    int tamanhoString = estadoAtual.length();
    string possivelEstadoFuturo;
    //Encontro essa posi��o para ver os estados futuros que s�o poss�veis com uma jogada.
    int indexDoElementoVazio = estadoAtual.find(posicaoVazia);
    int direita, esquerda, baixo, cima;

    //Encontrando a posi��o que pode haver troca com a posi��o vazia.
    direita = indexDoElementoVazio + 1;
    esquerda = indexDoElementoVazio - 1;
    baixo = indexDoElementoVazio + 3;
    cima = indexDoElementoVazio - 3;

    if(cima >= 0)
    {
        proximosEstados.push_back(trocarPosicaoDoElementoVazio(estadoAtual, indexDoElementoVazio, cima));
    }

    if(baixo < tamanhoString)
    {
        proximosEstados.push_back(trocarPosicaoDoElementoVazio(estadoAtual, indexDoElementoVazio, baixo));
    }

    //O index da direita n�o pode estar nas posi��es 0, 3 e 6, porque essas posi��es n�o pode ser a direita de nenhum elemento.
    if((direita % 3 != 0))
    {
        proximosEstados.push_back(trocarPosicaoDoElementoVazio(estadoAtual, indexDoElementoVazio, direita));
    }

    //O Elemento vazio n�o pode estar nas posi��es 0, 3 e 6, porque nessas posi��es n�o h� troca pelo elemento da esquerda.
    if((indexDoElementoVazio % 3) != 0)
    {
        proximosEstados.push_back(trocarPosicaoDoElementoVazio(estadoAtual, indexDoElementoVazio, esquerda));
    }

    return proximosEstados;
}

string JogoBase::trocarPosicaoDoElementoVazio(string estadoParaTrocar, int indexDoElementoVazio, int indexDoElementoParaTrocar)
{
    string novoEstado = estadoParaTrocar;
    novoEstado[indexDoElementoVazio] = estadoParaTrocar[indexDoElementoParaTrocar];
    novoEstado[indexDoElementoParaTrocar] = posicaoVazia[0];

    return novoEstado;
}

string JogoBase::definirProximoEstado(string estadoAtual)
{
    return 0;
}

bool JogoBase::verificarEstadoJaVisitado(string estadoParaVerificar, vector<string> * estadosJaVisitados)
{
    for(int i = 0; i < estadosJaVisitados->size(); i++)
    {
        if((*estadosJaVisitados)[i] == estadoParaVerificar)
        {
            return true;
        }
    }

    return false;
}

bool JogoBase::compararSolucao(string estadoParaComparar)
{
    if(estadoParaComparar == estadoSolucao)
    {
        return true;
    }

    return false;
}

vector<string> JogoBase::obterEstadosDaSolucao(No * estadoFinalSolucao)
{
    int nivelDaSolucao = estadoFinalSolucao->obterNivel();
    vector<string> estadosSolucao;
    No * estadoParaInserir = estadoFinalSolucao;

    for(int i = 0; i < nivelDaSolucao; i++)
    {
        estadosSolucao.push_back(estadoParaInserir->estadoAtual);
        estadoParaInserir = estadoParaInserir->obterPai();
    }

    return estadosSolucao;
}

vector<No*> JogoBase::criarNosFilhos(No * paiAtual)
{
    vector<string> proximosEstados = encontrarProximosEstados(paiAtual->estadoAtual);
    vector<No*> nosDosProximosEstados;

    for(int i = 0; i < proximosEstados.size(); i++)
    {
        No * proximoFilho = new No(proximosEstados[i], paiAtual);
        proximoFilho->determinarNivel();

        nosDosProximosEstados.push_back(proximoFilho);
    }

    return nosDosProximosEstados;
}
