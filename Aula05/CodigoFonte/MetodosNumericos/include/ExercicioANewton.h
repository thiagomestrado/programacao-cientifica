#ifndef EXERCICIOANEWTON_H
#define EXERCICIOANEWTON_H
#include "Newton.h"

class ExercicioANewton : public Newton
{
    public:
        double funcaoInicial(double valorDeX);
        double derivadaDaFuncaoInicial(double valorDeX);
};

#endif // EXERCICIOANEWTON_H
