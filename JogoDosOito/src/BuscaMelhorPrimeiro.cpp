#include <cmath>
#include "BuscaMelhorPrimeiro.h"

BuscaMelhorPrimeiro::BuscaMelhorPrimeiro(string _estadoInicial) : JogoBase(_estadoInicial)
{
}

No * BuscaMelhorPrimeiro::encontrarEstadoComMenorFuncao(int * indexDoEstadoDeMenorFuncao)
{
    No * elementoComOMenorValorDeFuncao = paraVisitar[0];
    *indexDoEstadoDeMenorFuncao = 0;

    for(int i = 1; i < paraVisitar.size(); i++)
    {
        //Para busca do A* deve ser obterValorDeF.
        if(paraVisitar[i]->valorDeH < elementoComOMenorValorDeFuncao->valorDeH)
        {
            elementoComOMenorValorDeFuncao = paraVisitar[i];
            *indexDoEstadoDeMenorFuncao = i;
        }
    }

    return elementoComOMenorValorDeFuncao;
}

void BuscaMelhorPrimeiro::determinarValorDeH(No * estadoParaDeterminarValorDeH)
{
    string estado = estadoParaDeterminarValorDeH->estadoAtual;
    int valor = 0;

    for(int i = 1; i < 10; i++)
    {
        int valorDoElemento;
        int xAtual, yAtual;
        if(i == 9)
        {
            xAtual = pontosDoEstadoSolucao[0]->X;
            yAtual = pontosDoEstadoSolucao[0]->Y;
            valorDoElemento = (int)estado.at(0) - 48; // -48 para converter de ASC
        }
        else
        {
            xAtual = pontosDoEstadoSolucao[i]->X;
            yAtual = pontosDoEstadoSolucao[i]->Y;
            valorDoElemento = (int)estado.at(i) - 48; // -48 para converter de ASC
        }

        int xSolucao = pontosDoEstadoSolucao[valorDoElemento]->X;
        int ySolucao = pontosDoEstadoSolucao[valorDoElemento]->Y;

        int distaciaX = abs(xSolucao - xAtual);
        int distanciaY = abs(ySolucao - yAtual);

        valor+= (distaciaX + distanciaY);
    }

    estadoParaDeterminarValorDeH->valorDeH = valor;
}
