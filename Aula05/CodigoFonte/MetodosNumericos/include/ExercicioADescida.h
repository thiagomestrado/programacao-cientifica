#ifndef EXERCICIOADESCIDA_H
#define EXERCICIOADESCIDA_H
#include "DescidaGradiente.h"

class ExercicioADescida : public DescidaGradiente
{
    public:
        double funcaoInicial(double valorDeX);
        double derivadaDaFuncaoInicial(double valorDeX);
};

#endif // EXERCICIOADESCIDA_H
