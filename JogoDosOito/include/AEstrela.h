#ifndef AESTRELA_H
#define AESTRELA_H
#include "JogoBase.h"
#include "MelhorPrimeiro.h"

class AEstrela : JogoBase
{
    public:
        AEstrela(string _estadoInicial);
        vector<string> encontrarSolucao();

    private:
        vector<string> jaVisitados;
        vector<No*> paraVisitar;
        bool verificarSeNovoCaminhoEhMenor(No * estadoParaVerificarCaminho); //M�todo �til quando utilizando A*, por�m para esse problema em espec�fico, o caminho novo nunca ser� melhor que o caminho j� encontrado at� aquele estado.
};

#endif // AESTRELA_H
