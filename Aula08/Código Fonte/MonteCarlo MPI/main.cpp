#include "mpi.h"
#include <iostream>
#include "include/Funcao.h"
#include <chrono>
#include <string>
#include <sstream>

using namespace std;
using namespace std::chrono;
#define MASTER 0

int main(int argc, char *argv[])
{
    int numeroDeTarefas, idDaTarefaAtual, quantidadeDeExecucoes;
    ostringstream converterAux;
    string idDaTarefaAtualTexto;

    //Inicialização dos parâmetros do MPI.
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numeroDeTarefas);
    MPI_Comm_rank(MPI_COMM_WORLD,&idDaTarefaAtual);

    /*string tarefaComecou = "A Tarefa ";
    converterAux << idDaTarefaAtual;
    idDaTarefaAtualTexto = converterAux.str();
    tarefaComecou += idDaTarefaAtualTexto + " comecou!!!\n";
    cout<<tarefaComecou;*/

    high_resolution_clock::time_point inicio = high_resolution_clock::now();

    //Definindo as possiveis funcoes para solucao que o usuario pode escolher
    Funcao::EquacaoEscolhida possiveisEquacoes[] = {&Funcao::funcaoAvaliar1, &Funcao::funcaoAvaliar2,
                                                    &Funcao::funcaoAvaliar3};

    int numeroDePontos, funcaoParaResolver;

    /*cout<<"Escolha a funcao que deseja resolver, 1 para Funcao1, 2 para Funcao2 e 3 para Toroide"<<endl;
    cin>>funcaoParaResolver;

    cout<<"Digite o numero de pontos: "<<endl;
    cin>>numeroDePontos;*/

    funcaoParaResolver = 1;
    numeroDePontos = 100;
    quantidadeDeExecucoes = 10000;

    try
    {
        double resultado, resultadoParcial;
        double erro = 0;
        Funcao *funcao = new Funcao(0, 1, funcaoParaResolver, numeroDePontos);

        for(int i = 0; i < quantidadeDeExecucoes; i++)
        {
            //Resultado da integral obtido por cada tarefa (Resultado Parcial).
            resultadoParcial = funcao->calcularIntegral(possiveisEquacoes[funcaoParaResolver-1], &erro);

            MPI_Reduce(&resultadoParcial, &resultado, 1, MPI_DOUBLE, MPI_SUM,
                       MASTER, MPI_COMM_WORLD);

            //Código usado para conferir os valores de teste.
            /*ostringstream parcialConverter;
            parcialConverter << resultadoParcial;
            string auxiliar = "Na tarefa " + idDaTarefaAtualTexto + " resultado parcial: " + parcialConverter.str() + "\n";
            cout<<auxiliar;**/

            //Se for a tarefa Master então deve-se calcular o valor final da integração.
            if(idDaTarefaAtual == MASTER)
            {
                resultado = resultado / numeroDeTarefas;
                //cout<<"O resultado obtido por Monte Carlo eh: "<<resultado<<endl;
                //cout<<"Com o erro: "<<erro<<endl;
            }
        }
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar resolver a Integral: "<<mensagemDeErro<<endl;
    }

    high_resolution_clock::time_point fim = high_resolution_clock::now();

    duration<double> duracao = duration_cast<duration<double>>(fim - inicio);
    /*ostringstream duracaoDaExecucacaoTexto;
    duracaoDaExecucacaoTexto << duracao.count();*/

    //Impressão do texto final mostrando quanto tempo a tarefa demorou para ser executada!
    /*string imprimirTempoExecucao = "A Tarefa " + idDaTarefaAtualTexto + " levou " + duracaoDaExecucacaoTexto.str() + " segundos para executar.\n";
    cout<<imprimirTempoExecucao;*/

    /*double somaDasTarefas;
    double duracaoDouble = duracao.count();
    MPI_Reduce(&duracaoDouble, &somaDasTarefas, 1, MPI_DOUBLE, MPI_SUM,
                       MASTER, MPI_COMM_WORLD);*/

    if(idDaTarefaAtual == MASTER)
    {
        //cout<<"Finalmente a soma dos tempos eh: "<<somaDasTarefas<<endl;
	cout<<duracao.count();
    }

    MPI_Finalize();
    return 0;
}
