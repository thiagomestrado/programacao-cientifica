#include "SubidaDaEncosta.h"

SubidaDaEncosta::SubidaDaEncosta(string _estadoInicial) : JogoBase(_estadoInicial)
{
}

vector<string> SubidaDaEncosta::encontrarSolucao()
{
    No * estadoSolucaoFinal = 0;
    No * noDoEstadoInicial = new No(estadoInicial);
    MelhorPrimeiro * melhorPrimeiro = new MelhorPrimeiro(); //Composição

    if(compararSolucao(estadoInicial))
    {
        vector<string> solucao;
        solucao.push_back(estadoInicial);

        return solucao;
    }

    No * visitando = noDoEstadoInicial;
    melhorPrimeiro->determinarValorDeH(visitando);

    while(estadoSolucaoFinal == 0)
    {
        vector<No *> filhos = criarNosFilhos(visitando);
        string estadoPai = visitando->estadoAtual;

        for(int i = 0; i < filhos.size(); i++)
        {
            No * filhoVisitando = filhos[i];

            if(compararSolucao(filhoVisitando->estadoAtual))
            {
                estadoSolucaoFinal = filhoVisitando;
                break;
            }

            melhorPrimeiro->determinarValorDeH(filhoVisitando);

            if(filhoVisitando->valorDeH < visitando->valorDeH)
            {
                visitando = filhoVisitando;
                break;
            }
        }

        if(visitando->estadoAtual == estadoPai)
        {
            break;
        }
    }

    if(estadoSolucaoFinal == 0)
    {
        throw "Solucao nao encontrada com Subida da Encosta!";
    }

    return obterEstadosDaSolucao(estadoSolucaoFinal);
}
