#include <iostream>
#include<cmath>
//Incluindo as classes que resolvem os problemas dos metodos numericos
#include "ExercicioANewton.h"
#include "ExercicioBNewton.h"
#include "ExercicioADescida.h"
#include "ExercicioBDescida.h"
#include "ExercicioCDescida.h"
#include "ExercicioDDescida.h"

using namespace std;

int main()
{
    double valorDeXNewtonA = 2.0; //X0 do exercicio A do metodo de Newton
    double valorDeXNewtonB = 2.0; // X0 do exercicio B do metodo de Newton

    double valorDeXDescidaA = 2.0; //X0 do exercicio A do m�todo de Descida
    double valorDeXDescidaB = 2.0; //X0 do exerc�cio B do m�todo de Descia
    double valorDeXDescidaC = 2;
    double valorDeYDescidaC = 2;
    double valorDeXDescidaD = 2;
    double valorDeYDescidaD = 2;

    int exercicioPraResolver;
    cout<<"Qual exercicio deseja resolver? 1 para A de Newton, 2 para B de Newton, 3 para A de Descida, 4 para B de Descida, 5 para C de Descida e 6 para D de Descida!"<<endl;
    //cin>>exercicioPraResolver;
    exercicioPraResolver = 4;
    cout<<exercicioPraResolver<<endl;

    double resultado;

    if(exercicioPraResolver == 1) //Exercicio A de Newton
    {
        ExercicioANewton exercicioANewton;
        try
        {
            resultado = exercicioANewton.obterResultado(valorDeXNewtonA);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }

    }
    else if(exercicioPraResolver == 2) //Exercicio B de Newton
    {
        ExercicioBNewton exercicioBNewton;
        try
        {
            resultado = exercicioBNewton.obterResultado(valorDeXNewtonB);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }
    }
    else if(exercicioPraResolver == 3) //Exercicio A de Descida
    {
        ExercicioADescida exercicioADescida;
        try
        {
            resultado = exercicioADescida.obterResultado(valorDeXDescidaA);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }
    }
    else if(exercicioPraResolver == 4) //Exercicio B de Newton
    {
        ExercicioBDescida exercicioBDescida;
        try
        {
            resultado = exercicioBDescida.obterResultado(valorDeXDescidaB);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }
    }
    else if(exercicioPraResolver == 5) //Exercicio B de Newton
    {
        ExercicioCDescida exercicioCDescida;
        try
        {
            resultado = exercicioCDescida.obterResultado(valorDeXDescidaC, valorDeYDescidaC);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }
    }
    else if(exercicioPraResolver == 6) //Exercicio B de Newton
    {
        ExercicioDDescida exercicioDDescida;
        try
        {
            resultado = exercicioDDescida.obterResultado(valorDeXDescidaD, valorDeYDescidaD);
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
            return 0;
        }
    }

    cout<<"O Resultado final eh: "<<resultado<<endl;
    return 0;
}
