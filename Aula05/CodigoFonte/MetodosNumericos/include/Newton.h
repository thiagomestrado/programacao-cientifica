#ifndef NEWTON_H
#define NEWTON_H
#include <iostream>
#include <cmath>
using namespace std;

class Newton
{
    public:
        virtual double funcaoInicial(double valorDeX) = 0;
        virtual double derivadaDaFuncaoInicial(double valorDeX) = 0;
        double obterResultado(double valorDeX);

    private:
        double taxaDeAprendizado = 1;
        int numeroMaximoDePassos = 200; //Quantidade maxima de iteracoes.
        double criterioDeParada = 0.0001; //Quer dizer que esta proximo suficiente de zero para desconsiderar mais iteracoes.
};

#endif // NEWTON_H
