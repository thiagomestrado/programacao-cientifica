#ifndef FUNCAO_H
#define FUNCAO_H
#include <cmath>
#include <random>
#include <ctime>
#include <iostream>

using namespace std;

class Funcao
{
    public:
        Funcao(double inicioIntegral, double fimIntegral, int funcaoEscolhida, int numeroDePontos);

        //Definindo o tipo para ser usado no array de ponteiros para funcoes (as funcoes serao as equacoes que devem ser resolvidas).
        typedef double (Funcao::*EquacaoEscolhida)(double valorDeX);

        //Metodos que representam as funcoes de base que servirao para avaliacao da integral.
        double funcaoAvaliar1(double valorX); //M�todo que retorna valor da funcao exercicio 1
        double funcaoAvaliar2(double valorX);//M�todo que retorna valor da funcao exercicio 2
        double funcaoAvaliar3(double valorX);//Metodo que retorna o valor da funcao do toroide.

        //Metodo principal que resolvera a integral usando Monte Carlo
        double calcularIntegral(EquacaoEscolhida funcaoResolver, double *erro);

    private:
        double inicioIntegral; //Limite inicial da integral.
        double fimIntegral; //Limite final da integral.
        int funcaoEscolhida; //funcao que sera resolvida e que foi escolhida pelo usuario.
        int numeroDePontos;
        uniform_int_distribution<int> *distribuicao;//(0, 99);
        mt19937 *aleatorio;

        double gerarNumeroAleatorio(); //Metodo responsavel pela geracao de numeros aleatorios.

        uniform_int_distribution<int> *distribuicaoY;
        uniform_int_distribution<int> *distribuicaoZ;
        mt19937 *aleatorioY;
        mt19937 *aleatorioZ;
};

#endif // FUNCAO_H
