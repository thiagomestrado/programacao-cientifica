#include <iostream>
#include "Funcao.h"

using namespace std;

int main()
{
    //Instanciando as fun��es, uma vari�vel para cada uma das fun��es que temos. Passando o intervalo da integral e tamb�m qual fun��o �.
    //Fun��o Integral 1: ((e^x) * dx)
    //Fun��o Integral 2: ((raiz(1 - x^2)) * dx)
    //Fun��o Integral 3: ((e^(-x^2)) * dx)

    int metodoEscolhido;
    int funcaoEscolhidaAtual;

    cout<<"Qual funcao deseja resolver. 1 para Func1, 2 para Func2 e 3 para Func3"<<endl;
    cin>>funcaoEscolhidaAtual;

    cout<<"Escolha o metodo para resolver as integrais. 1 para Retangulo, 2 para Simpson, 3 para Trapezio e ";
    cout<<"4 para Quadratura Adapatativa"<<endl;
    cin>>metodoEscolhido;

    Funcao funcao(0, 1, funcaoEscolhidaAtual, metodoEscolhido, 100);

    typedef double (Funcao::*metodosIntegral)(vector<double>, double *);
    metodosIntegral metodosParaCalculoIntegral[] = {&Funcao::integracaoRetangulo, &Funcao::integracaoSimpson,
                                                    &Funcao::integracaoTrapezio};

    try
    {
        double resultado;
        double erro = 0;

        resultado = funcao.calcularIntegral((metodosParaCalculoIntegral[metodoEscolhido-1]), 100, &erro);

        cout<<"O resultado da integral numerica eh: "<<resultado<<endl;
        cout<<"Com o erro: "<<erro<<endl;
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar resolver a Integral: "<<mensagemDeErro<<endl;
    }

    return 0;
}
