#include "MelhorPrimeiro.h"

MelhorPrimeiro::MelhorPrimeiro()
{
    determinarPosicoesDasPecasNaListaDeSolucao();
}

MelhorPrimeiro::~MelhorPrimeiro()
{
    pontosDoEstadoSolucao.erase(pontosDoEstadoSolucao.begin(), pontosDoEstadoSolucao.end());
}

void MelhorPrimeiro::determinarValorDeH(No * estadoParaDeterminarValorDeH)
{
    string estado = estadoParaDeterminarValorDeH->estadoAtual;
    int valor = 0;

    for(int i = 1; i < 10; i++)
    {
        int valorDoElemento;
        int xAtual, yAtual;
        if(i == 9)
        {
            xAtual = pontosDoEstadoSolucao[0]->X;
            yAtual = pontosDoEstadoSolucao[0]->Y;
        }
        else
        {
            xAtual = pontosDoEstadoSolucao[i]->X;
            yAtual = pontosDoEstadoSolucao[i]->Y;
        }

        valorDoElemento = (int)estado.at(i-1) - 48; // -48 para converter de ASC

        int xSolucao = pontosDoEstadoSolucao[valorDoElemento]->X;
        int ySolucao = pontosDoEstadoSolucao[valorDoElemento]->Y;

        int distaciaX = abs(xSolucao - xAtual);
        int distanciaY = abs(ySolucao - yAtual);

        valor += (distaciaX + distanciaY);
    }

    estadoParaDeterminarValorDeH->valorDeH = valor;
}

No * MelhorPrimeiro::verificarEstadoNaListaParaVisitar(string estadoParaVerificar, vector<No*> * listaParaFazerVerificao)
{
    for(int i = 0; i < listaParaFazerVerificao->size(); i++)
    {
        if(listaParaFazerVerificao->at(i)->estadoAtual == estadoParaVerificar)
        {
            return listaParaFazerVerificao->at(i);
        }
    }

    return 0;
}

No * MelhorPrimeiro::encontrarEstadoComMenorFuncao(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados, bool buscaGulosa)
{
    if(buscaGulosa)
    {
        return encontrarEstadoComMenorFuncaoBuscaGulosa(indexDoEstadoDeMenorFuncao, listaDeEstadosVisitados);
    }
    else // Ent�o ser� utilizado A*
    {
        return encontrarEstadoComMenorFuncaoAEstrela(indexDoEstadoDeMenorFuncao, listaDeEstadosVisitados);
    }
}

No * MelhorPrimeiro::encontrarEstadoComMenorFuncaoBuscaGulosa(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados)
{
    No * elementoComOMenorValorDeFuncao = listaDeEstadosVisitados->at(0);
    *indexDoEstadoDeMenorFuncao = 0;

    for(int i = 1; i < listaDeEstadosVisitados->size(); i++)
    {
        //Diferente do que ocorre com A*, na Busca Gulosa n�o � considerada a dist�ncia at� aquele n�, s� o valor do H mesmo.
        if(listaDeEstadosVisitados->at(i)->valorDeH < elementoComOMenorValorDeFuncao->valorDeH)
        {
            elementoComOMenorValorDeFuncao = listaDeEstadosVisitados->at(i);
            *indexDoEstadoDeMenorFuncao = i;
        }
    }

    return elementoComOMenorValorDeFuncao;
}

No * MelhorPrimeiro::encontrarEstadoComMenorFuncaoAEstrela(int * indexDoEstadoDeMenorFuncao, vector<No*> *listaDeEstadosVisitados)
{
    No * elementoComOMenorValorDeFuncao = listaDeEstadosVisitados->at(0);
    *indexDoEstadoDeMenorFuncao = 0;

    for(int i = 1; i < listaDeEstadosVisitados->size(); i++)
    {
        if(listaDeEstadosVisitados->at(i)->obterValorDeF() < elementoComOMenorValorDeFuncao->obterValorDeF())
        {
            elementoComOMenorValorDeFuncao = listaDeEstadosVisitados->at(i);
            *indexDoEstadoDeMenorFuncao = i;
        }
    }

    return elementoComOMenorValorDeFuncao;
}

void MelhorPrimeiro::determinarPosicoesDasPecasNaListaDeSolucao()
{
    pontosDoEstadoSolucao.push_back(new Ponto(3,1));
    pontosDoEstadoSolucao.push_back(new Ponto(1,3));
    pontosDoEstadoSolucao.push_back(new Ponto(2,3));
    pontosDoEstadoSolucao.push_back(new Ponto(3,3));
    pontosDoEstadoSolucao.push_back(new Ponto(1,2));
    pontosDoEstadoSolucao.push_back(new Ponto(2,2));
    pontosDoEstadoSolucao.push_back(new Ponto(3,2));
    pontosDoEstadoSolucao.push_back(new Ponto(1,1));
    pontosDoEstadoSolucao.push_back(new Ponto(2,1));
}

