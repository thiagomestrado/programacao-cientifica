#include "BFS.h"

BFS::BFS(string _estadoInicial) : JogoBase(_estadoInicial)
{
}

vector<string> BFS::encontrarSolucao()
{
    vector<No*> paraVisitar;
    No * estadoSolucaoFinal;
    vector<string> estadosJaVisitados;

    if(compararSolucao(estadoInicial))
    {
        vector<string> solucao;
        solucao.push_back(estadoInicial);

        return solucao;
    }

    paraVisitar.push_back(paiInicial);
    bool solucaoEncontrada = false;

    while(paraVisitar.size() > 0 && !solucaoEncontrada)
    {
        No * paiAtual = paraVisitar[0];
        paraVisitar.erase(paraVisitar.begin()); //Apaga sempre o primeiro elemento
        vector<No*> filhosParaVisitar = criarNosFilhos(paiAtual);

        for(int i = 0; i < filhosParaVisitar.size() && !solucaoEncontrada; i++)
        {
            string filhoAtualParaVisitar = filhosParaVisitar[i]->estadoAtual;

            if(!verificarEstadoJaVisitado(filhoAtualParaVisitar, &estadosJaVisitados))
            {
                if(compararSolucao(filhoAtualParaVisitar))
                {
                    estadoSolucaoFinal = filhosParaVisitar[i];
                    solucaoEncontrada = true;
                }
                else
                {
                    paraVisitar.push_back(filhosParaVisitar[i]);
                }
            }
        }

        estadosJaVisitados.push_back(paiAtual->estadoAtual);
    }

    paraVisitar.clear();
    estadosJaVisitados.clear();

    return obterEstadosDaSolucao(estadoSolucaoFinal);;
}
