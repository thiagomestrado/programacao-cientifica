#ifndef BFS_H
#define BFS_H
#include "JogoBase.h"

class BFS : JogoBase
{
    public:
        BFS(string _estadoInicial);
        vector<string> encontrarSolucao();
};

#endif // BFS_H
