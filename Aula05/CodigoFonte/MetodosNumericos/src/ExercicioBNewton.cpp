#include "ExercicioBNewton.h"

double ExercicioBNewton::funcaoInicial(double valorDeX)
{
    double primeiroTermoDaFuncao = pow(valorDeX,3.0);
    double segundoTermoDaFuncao = pow(valorDeX, 2.0);
    return (primeiroTermoDaFuncao - (2 * segundoTermoDaFuncao) + 2);
}

double ExercicioBNewton::derivadaDaFuncaoInicial(double valorDeX)
{
    double derivadaPrimeiroTermo = 3 * pow(valorDeX,2);
    double derivadaSegundoTermo = 4 * valorDeX;
    return (derivadaPrimeiroTermo - derivadaSegundoTermo);
}
