#ifndef EXERCICIODDESCIDA_H
#define EXERCICIODDESCIDA_H
#include "DescidaGradiente.h"
#include<limits>

using namespace std;

class ExercicioDDescida
{
    public:
        double funcaoInicial(double valorDeX, double valorDeY);
        double derivadaX(double valorDeX, double valorDeY);
        double derivadaY(double valorDeX, double valorDeY);
        double obterResultado(double valorDeX, double valorDeY);
        double valorAux = 0;

    private:
        double taxaDeAprendizado = 0.1;
        int numeroMaximoDePassos = 30; //Quantidade maxima de iteracoes.
        double criterioDeParada = 0.0001; //Quer dizer que esta proximo suficiente de zero para desconsiderar mais iteracoes.
};

#endif // EXERCICIODDESCIDA_H
