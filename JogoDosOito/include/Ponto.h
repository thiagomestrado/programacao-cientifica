#ifndef PONTO_H
#define PONTO_H

class Ponto
{
    public:
        Ponto(int _x, int _y);
        int X;
        int Y;
};

#endif // PONTO_H
