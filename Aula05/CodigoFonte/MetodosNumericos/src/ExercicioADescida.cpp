#include "ExercicioADescida.h"

double ExercicioADescida::funcaoInicial(double valorDeX)
{
    return valorDeX * valorDeX;
}

double ExercicioADescida::derivadaDaFuncaoInicial(double valorDeX)
{
    return (2 * valorDeX);
}
