#ifndef EXERCICIOBNEWTON_H
#define EXERCICIOBNEWTON_H
#include "Newton.h"

class ExercicioBNewton : public Newton
{
    public:
        double funcaoInicial(double valorDeX);
        double derivadaDaFuncaoInicial(double valorDeX);
};

#endif // EXERCICIOBNEWTON_H
