#include <iostream>
#include <string>
#include <vector>
#include "JogoBase.h"
#include "BFS.h"
#include "DFS.h"
#include "AEstrela.h"
#include "BuscaGulosa.h"
#include "SubidaDaEncosta.h"
using namespace std;

int main()
{
    int algoritmoEscolhido;

    //Estados usados Para testes.
    //string estadoInicial = "013425786";
    //string estadoInicial = "713260548";
    //string estadoInicial = "423601758";
    //string estadoInicial = "237548061";
    //string estadoInicial = "023145786";
    string estadoInicial = "183465702";

    cout<<"Escolha o algoritmo que deseja executar"<<endl;
    cout<<"1 para BFS, 2 para DFS, 3 para A*, 4 para Best-First(Busca Gulosa) e 5 para Subida da Encosta"<<endl;
    cin>>algoritmoEscolhido;

    if(algoritmoEscolhido == 1) //Busca em Largura
    {
        BFS *bfs = new BFS(estadoInicial);
        vector<string> todosEstadosAteSolucao = bfs->encontrarSolucao();

        cout<<"Com o Estado Inicial: "<<estadoInicial<<" Devem ser seguidos os passos abaixo!"<<endl;

        for(int i = todosEstadosAteSolucao.size() - 1, contadorAuxiliar=1; i >= 0; i--, contadorAuxiliar++)
        {
            cout<<"Passo "<<contadorAuxiliar<<" eh: "<<todosEstadosAteSolucao[i]<<endl;
        }
    }
    else if(algoritmoEscolhido == 2) //Busca em Profundidade
    {
        DFS *dfs = new DFS(estadoInicial);
        try
        {
            vector<string> todosEstadosAteSolucao = dfs->encontrarSolucao();

            cout<<"Com o Estado Inicial: "<<estadoInicial<<" Devem ser seguidos os passos abaixo!"<<endl;

            for(int i = todosEstadosAteSolucao.size() - 1, contadorAuxiliar=1; i >= 0; i--, contadorAuxiliar++)
            {
                cout<<"Passo "<<contadorAuxiliar<<" eh: "<<todosEstadosAteSolucao[i]<<endl;
            }
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Ocorreu um erro ao tentar encontrar solucao: "<<mensagemDeErro<<endl;
        }
    }
    else if(algoritmoEscolhido == 3) //A*
    {
        AEstrela *aestrela = new AEstrela(estadoInicial);
        try
        {
            vector<string> todosEstadosAteSolucao = aestrela->encontrarSolucao();

            cout<<"Com o Estado Inicial: "<<estadoInicial<<" Devem ser seguidos os passos abaixo!"<<endl;

            for(int i = todosEstadosAteSolucao.size() - 1, contadorAuxiliar=1; i >= 0; i--, contadorAuxiliar++)
            {
                cout<<"Passo "<<contadorAuxiliar<<" eh: "<<todosEstadosAteSolucao[i]<<endl;
            }
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Ocorreu um erro ao tentar encontrar solucao: "<<mensagemDeErro<<endl;
        }
    }
    else if(algoritmoEscolhido == 4) //Best-First - Busca Gulosa
    {
        BuscaGulosa *buscaGulosa = new BuscaGulosa(estadoInicial);
        try
        {
            vector<string> todosEstadosAteSolucao = buscaGulosa->encontrarSolucao();

            cout<<"Com o Estado Inicial: "<<estadoInicial<<" Devem ser seguidos os passos abaixo!"<<endl;

            for(int i = todosEstadosAteSolucao.size() - 1, contadorAuxiliar=1; i >= 0; i--, contadorAuxiliar++)
            {
                cout<<"Passo "<<contadorAuxiliar<<" eh: "<<todosEstadosAteSolucao[i]<<endl;
            }
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Ocorreu um erro ao tentar encontrar solucao: "<<mensagemDeErro<<endl;
        }
    }
    else if(algoritmoEscolhido == 5) //Subida da Encosta
    {
        SubidaDaEncosta *subidaDaEncosta = new SubidaDaEncosta(estadoInicial);
        try
        {
            vector<string> todosEstadosAteSolucao = subidaDaEncosta->encontrarSolucao();

            cout<<"Com o Estado Inicial: "<<estadoInicial<<" Devem ser seguidos os passos abaixo!"<<endl;

            for(int i = todosEstadosAteSolucao.size() - 1, contadorAuxiliar=1; i >= 0; i--, contadorAuxiliar++)
            {
                cout<<"Passo "<<contadorAuxiliar<<" eh: "<<todosEstadosAteSolucao[i]<<endl;
            }
        }
        catch(char const * mensagemDeErro)
        {
            cout<<"Ocorreu um erro ao tentar encontrar solucao: "<<mensagemDeErro<<endl;
        }
    }
    else
    {
        cout<<"Operacao nao suportada! Tente novamente, por favor!"<<endl;
    }

    return 0;
}
