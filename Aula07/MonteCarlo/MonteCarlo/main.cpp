#include <iostream>
#include "Funcao.h"

using namespace std;

int main()
{
    //Definindo as possiveis funcoes para solucao que o usuario pode escolher
    Funcao::EquacaoEscolhida possiveisEquacoes[] = {&Funcao::funcaoAvaliar1, &Funcao::funcaoAvaliar2,
                                                    &Funcao::funcaoAvaliar3};

    int numeroDePontos, funcaoParaResolver;

    cout<<"Escolha a funcao que deseja resolver, 1 para Funcao1, 2 para Funcao2 e 3 para Toroide"<<endl;
    cin>>funcaoParaResolver;

    cout<<"Digite o numero de pontos: "<<endl;
    cin>>numeroDePontos;

    try
    {
        double resultado;
        double erro = 0;
        Funcao *funcao = new Funcao(0, 1, funcaoParaResolver, numeroDePontos);

        resultado = funcao->calcularIntegral(possiveisEquacoes[funcaoParaResolver-1], &erro);

        cout<<"O resultado obtido por Monte Carlo eh: "<<resultado<<endl;
        cout<<"Com o erro: "<<erro<<endl;
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar resolver a Integral: "<<mensagemDeErro<<endl;
    }

    return 0;
}
