#include "DFS.h"

DFS::DFS(string _estadoInicial) : JogoBase(_estadoInicial)
{
}

const int DFS::profundidadeMaxima = 30;

vector<string> DFS::encontrarSolucao()
{
    No * estadoSolucaoFinal = 0;
    No * noDoEstadoInicial = new No(estadoInicial);
    vector<string> estadosJaVisitados;

    if(compararSolucao(estadoInicial))
    {
        vector<string> solucao;
        solucao.push_back(estadoInicial);

        return solucao;
    }

    estadoSolucaoFinal = visitarFilhosDiretos(noDoEstadoInicial);

    if(estadoSolucaoFinal == 0)
    {
        throw "Solucao nao encontrada!";
    }

    return obterEstadosDaSolucao(estadoSolucaoFinal);
}

No* DFS::visitarFilhosDiretos(No * pai)
{
    if(compararSolucao(pai->estadoAtual))
    {
        return pai;
    }

    if(verificarEstadoJaVisitado(pai->estadoAtual, &jaVisitados) || pai->obterNivel() >= profundidadeMaxima)
    {
        return 0;
    }

    jaVisitados.push_back(pai->estadoAtual);

    vector<No*> filhosParaVisitar = criarNosFilhos(pai);
    for(int i = 0; i < filhosParaVisitar.size(); i++)
    {
        No * filhoParaVisitar = filhosParaVisitar[i];
        No * resultadoVisita = visitarFilhosDiretos(filhoParaVisitar);
        if(resultadoVisita != 0)
        {
            return resultadoVisita;
        }
    }

    return 0;
}
