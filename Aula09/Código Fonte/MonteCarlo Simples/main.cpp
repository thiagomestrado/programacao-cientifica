#include <iostream>
#include "include/Funcao.h"
#include <chrono>
#include <string>
#include <sstream>

using namespace std;
using namespace std::chrono;
#define MASTER 0

int main(int argc, char *argv[])
{
    int numeroDeTarefas, idDaTarefaAtual, quantidadeDeExecucoes;

    high_resolution_clock::time_point inicio = high_resolution_clock::now();

    //Definindo as possiveis funcoes para solucao que o usuario pode escolher
    Funcao::EquacaoEscolhida possiveisEquacoes[] = {&Funcao::funcaoAvaliar1, &Funcao::funcaoAvaliar2,
                                                    &Funcao::funcaoAvaliar3};

    int numeroDePontos, funcaoParaResolver;

    funcaoParaResolver = 3;
    numeroDePontos = 10000;
    quantidadeDeExecucoes = 10000;
    numeroDeTarefas = 4;

    try
    {
        double resultado, somaResultado;
        double erro = 0;
        Funcao *funcao = new Funcao(0, 1, funcaoParaResolver, numeroDePontos);

        for(int i = 0; i < quantidadeDeExecucoes; i++)
        {
            for(int j = 0; j < numeroDeTarefas; j++)
            {
                        //Resultado da integral obtido por cada tarefa (Resultado Parcial).
                resultado = funcao->calcularIntegral(possiveisEquacoes[funcaoParaResolver-1], &erro);
                somaResultado += resultado;

                somaResultado = 0;
                if(j == 1)
                {
                    somaResultado = somaResultado / numeroDeTarefas;
                }
            }
        }
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar resolver a Integral: "<<mensagemDeErro<<endl;
    }

    high_resolution_clock::time_point fim = high_resolution_clock::now();
    duration<double> duracao = duration_cast<duration<double>>(fim - inicio);

    cout<<"Tempo de execucao: "<<duracao.count()<<endl;

    return 0;
}
