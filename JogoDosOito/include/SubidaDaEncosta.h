#ifndef SUBIDADAENCOSTA_H
#define SUBIDADAENCOSTA_H
#include "JogoBase.h"
#include "MelhorPrimeiro.h"

class SubidaDaEncosta : JogoBase
{
    public:
        SubidaDaEncosta(string _estadoInicial);
        vector<string> encontrarSolucao();
};

#endif // SUBIDADAENCOSTA_H
