#include "ExercicioANewton.h"

double ExercicioANewton::funcaoInicial(double valorDeX)
{
    return valorDeX * valorDeX;
}

double ExercicioANewton::derivadaDaFuncaoInicial(double valorDeX)
{
    return (2 * valorDeX);
}
