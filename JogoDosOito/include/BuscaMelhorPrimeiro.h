#ifndef BUSCAMELHORPRIMEIRO_H
#define BUSCAMELHORPRIMEIRO_H
#include "JogoBase.h"

class BuscaMelhorPrimeiro : JogoBase
{
    public:
        BuscaMelhorPrimeiro(string _estadoInicial);

    protected:
        vector<string> jaVisitados;
        vector<No*> paraVisitar;
        No* encontrarEstadoComMenorFuncao(int * indexDoEstadoDeMenorFuncao); //M�todo respons�vel por encontrar o N� com o menor valor da fun��o heur�stica.
        void determinarValorDeH(No * estadoParaDeterminarADistancia); //M�todo respons�vel por encontrar o valor de H utilizando a dist�ncia de Manhattan.
        bool verificarSeNovoCaminhoEhMenor(No * estadoParaVerificarCaminho);
        No * verificarEstadoNaListaParaVisitar(string estadoParaVerificar);
};

#endif // BUSCAMELHORPRIMEIRO_H
