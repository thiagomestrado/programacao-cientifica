#ifndef JOGOBASE_H
#define JOGOBASE_H
#include <string>
#include <vector>
#include "No.h"
#include "Ponto.h"
using namespace std;

class JogoBase
{
    public:
        JogoBase(string _estadoInicial);
        virtual ~JogoBase();
        //M�todo deve ser implementado pelas classes que herdam desta classe.
        virtual vector<string> encontrarSolucao() = 0;

    protected:
        static const string estadoSolucao;
        static const string posicaoVazia;
        No * paiInicial;
        string estadoInicial;
        string definirProximoEstado(string estadoAtual);
        vector<string> encontrarProximosEstados(string estadoAtual);
        bool verificarEstadoJaVisitado(string estadoParaVerificar, vector<string> * estadosJaVisitados);
        string trocarPosicaoDoElementoVazio(string estadoParaTrocar, int indexDoElementoVazio, int indexDoElementoParaTrocar);
        bool compararSolucao(string estadoParaComparar);
        vector<string> obterEstadosDaSolucao(No * estadoFinalSolucao);
        vector<No *> criarNosFilhos(No * paiAtual);
};

#endif // JOGOBASE_H
