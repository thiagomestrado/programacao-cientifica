#ifndef FUNCAO_H
#define FUNCAO_H
#include<math.h>
#include<vector>
#include <iostream>

using namespace std;

class Funcao
{
    public:
        Funcao(double inicioIntegral, double fimIntegral, int funcao, int metodoEscolhido, int numeroDivisoes);
        typedef double (Funcao::*metodosIntegral)(vector<double>, double *);

        //Metodo principal que sera chamado para resolver a integral numerica.
        double calcularIntegral(metodosIntegral metodo, int numeroDivisoes, double *erro);

        //Metodos para os metodos de resolucao de integral numerica.
        double integracaoRetangulo(vector<double> valoresX, double *erro);
        double integracaoTrapezio(vector<double> valoresX, double *erro);
        double integracaoSimpson(vector<double> valoresX, double *erro);

        //Metodos que representam a derivada das funcoes que serao avaliadas. Essas derivadas serao usadas nos calculos dos erros.
        double derivadaF1(double valorX); //M�todo que retorna valor da derivada de e^x
        double derivadaF2(double valorX); //M�todo que retorna valor da derivada de ((raiz(1 - x^2)) * dx)
        double derivadaF3(double valorX); //M�todo que retorna valor da derivada de ((e^(-x^2)) * dx)

        //Metodos que representam as funcoes basem que servirao para avaliacao da integral.
        double funcaoAvaliar1(double valorX); //M�todo que retorna valor da funcao e^x
        double funcaoAvaliar2(double valorX);//M�todo que retorna valor da funcao ((raiz(1 - x^2)) * dx)
        double funcaoAvaliar3(double valorX);//M�todo que retorna valor da funcao ((e^(-x^2)) * dx)

    private:
        double inicioIntegral; //Limite inicial da integral.
        double fimIntegral; //Limite final da integral.

        int funcaoEscolhida; //funcao que sera resolvida e que foi escolhida pelo usuario.
        int metodoEscolhido; //metodo que foi escolhido pelo usuario para solucioanr a integral.

        double definirFuncaoParaResolver(double valorX);
        double definirFuncaoDerivadaErro(double valorX);

        int numeroDivisoes;
};

#endif // FUNCAO_H
